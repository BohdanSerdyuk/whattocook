import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization") version ("1.3.61")
    kotlin("kapt")
}

kotlin {
    kapt {
        correctErrorTypes=true
    }
    //select iOS target platform depending on the Xcode environment variables
    val iOSTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget =
        if (System.getenv("SDK_NAME")?.startsWith("iphoneos") == true)
            ::iosArm64
        else
            ::iosX64

    iOSTarget("ios") {
        binaries {
            framework {
                baseName = "SharedData"
            }
        }
    }

    jvm("android")



    sourceSets {
        all {
            languageSettings.useExperimentalAnnotation("kotlin.Experimental")
        }
    }

    // Libraries versions
    val ktorVersion = "1.2.6"
    val coroutinesVersion = "1.3.2"
    val serializationVersion = "0.14.0"
    sourceSets["commonMain"].dependencies {
        implementation("org.jetbrains.kotlin:kotlin-stdlib-common")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:0.14.0")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:1.3.3")

        // HTTP
        implementation("io.ktor:ktor-client-core:1.6.1")
        implementation("io.ktor:ktor-client-json:1.6.1")
        implementation("io.ktor:ktor-client-serialization:1.6.1")
    }

    sourceSets["androidMain"].dependencies {
        implementation("org.jetbrains.kotlin:kotlin-stdlib")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.5.0")

        // HTTP
        implementation("io.ktor:ktor-client-android:1.6.1")
        implementation("io.ktor:ktor-client-json-jvm:1.6.1")
        implementation("io.ktor:ktor-client-serialization-jvm:1.6.1")

        //Image load and cache
//        val glideVersion = "4.11.0"
//        configurations.get("kapt").dependencies.add(
//            org.gradle.api.internal.artifacts.dependencies.DefaultExternalModuleDependency(
//                "com.github",
//                "bumptech.glide:compiler:",
//                glideVersion
//            )
//        )
    }

    sourceSets["iosMain"].dependencies {
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:1.3.3")

        // HTTP
        implementation("io.ktor:ktor-client-ios:1.6.1")
        implementation("io.ktor:ktor-client-json-native:1.3.0")
        implementation("io.ktor:ktor-client-serialization-iosx64:1.6.1")
    }
}

val packForXcode by tasks.creating(Sync::class) {
    val targetDir = File(buildDir, "xcode-frameworks")

    /// selecting the right configuration for the iOS
    /// framework depending on the environment
    /// variables set by Xcode build
    val mode = System.getenv("CONFIGURATION") ?: "DEBUG"
    val framework = kotlin.targets
        .getByName<KotlinNativeTarget>("ios")
        .binaries.getFramework(mode)
    inputs.property("mode", mode)
    dependsOn(framework.linkTask)

    from({ framework.outputDirectory })
    into(targetDir)

    /// generate a helpful ./gradlew wrapper with embedded Java path
    doLast {
        val gradlew = File(targetDir, "gradlew")
        gradlew.writeText(
            "#!/bin/bash\n"
                    + "export 'JAVA_HOME=${System.getProperty("java.home")}'\n"
                    + "cd '${rootProject.rootDir}'\n"
                    + "./gradlew \$@\n"
        )
        gradlew.setExecutable(true)
    }
}

tasks.getByName("build").dependsOn(packForXcode)