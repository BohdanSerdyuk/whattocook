package image_loader.image_loader

import common.BaseRestApiImpl
import image_loader.models.Image
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import io.ktor.client.response.readBytes
import io.ktor.http.HttpMethod
import io.ktor.http.URLProtocol

@Suppress("EXPERIMENTAL_API_USAGE")
class ImageLoaderApi(clientEngine: HttpClientEngine): BaseRestApiImpl() {
    /**
     * Http client creation for this request
     * @param clientEngine - real engine
     */
    private val client = HttpClient(clientEngine) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    suspend fun fetchImage(urlString: String): ImageLoaderResponse {
        val response = client.get<ByteArray>(urlString)
        return ImageLoaderResponse(Image(response))
    }
}