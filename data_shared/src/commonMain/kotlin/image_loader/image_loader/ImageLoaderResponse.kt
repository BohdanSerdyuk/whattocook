package image_loader.image_loader

import image_loader.models.Image

data class ImageLoaderResponse(val image: Image)