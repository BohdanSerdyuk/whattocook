package image_loader.models

data class Image(val bytes: ByteArray)