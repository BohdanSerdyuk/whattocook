package firebase_remote_configs.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LabelsFilter(
    @SerialName(value = "name") val name: String
)