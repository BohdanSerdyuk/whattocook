package google_cloud.labelsDetection

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

data class LabelsDetectionRequest(val requests: ByteArray)

@Serializable
data class Request(@SerialName(value = "image") val image: Image, @SerialName(value = "features") val features: List<Feature>)

@Serializable
data class Image(@SerialName(value = "content") val content: String)

@Serializable
data class Feature(@SerialName(value = "maxResults") val maxResults: Int, @SerialName(value = "type") val type: String)

enum class DetectionTypes(val type: String) {
    LABEL_DETECTION("LABEL_DETECTION")
}