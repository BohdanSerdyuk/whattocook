package google_cloud.labelsDetection

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LabelsDetectionResponseItem(@SerialName("title") val description: String)