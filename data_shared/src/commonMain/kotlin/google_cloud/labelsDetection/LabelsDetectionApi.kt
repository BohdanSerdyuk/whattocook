package edamam.recipeCards

import common.BaseRestApiImpl
import common.RemoteContract
import common.RemoteContract.Companion.CLOUD_VISION_KEY
import google_cloud.labelsDetection.LabelsDetectionRequest
import google_cloud.labelsDetection.LabelsDetectionResponseItem
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.response.readText
import io.ktor.http.*
import io.ktor.utils.io.core.*

@Suppress("EXPERIMENTAL_API_USAGE")
class LabelsDetectionApi(clientEngine: HttpClientEngine): BaseRestApiImpl() {

    /**
     * Http client creation for this request
     * @param clientEngine - real engine
     */
    private val client = HttpClient(clientEngine) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    suspend fun fetchDetectedLabels(labelsDetectionRequest: LabelsDetectionRequest): List<LabelsDetectionResponseItem> {
        val response = client.post<String> {
            url {
                protocol = URLProtocol.HTTP
                host = RemoteContract.cloudVisionUrl
//                encodedPath = "images:annotate?key=${CLOUD_VISION_KEY}"
                body = MultiPartFormDataContent(
                    formData {
                            this.appendInput(
                                key = "file",
                                headers = Headers.build {
                                    append(HttpHeaders.ContentDisposition,
                                        "filename=image")
                                },
                                size = labelsDetectionRequest.requests.size.toLong()
                            ) { buildPacket { writeFully(labelsDetectionRequest.requests) } }
                    }
                )

            }
        }
        val result = response
        try {
            val dff = decodeFromString<List<LabelsDetectionResponseItem>>(response)
            println(dff)
            println(dff)
            println(dff)
            println(dff)
        }
        catch (e: Exception) {
            println(e)
            println(e)

        }
        println(result)
        println(result)
        println(result)

        return decodeFromString(result)
    }
}