package google_cloud.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CloudVisionLabel(
    @SerialName(value = "description") val name: String
)