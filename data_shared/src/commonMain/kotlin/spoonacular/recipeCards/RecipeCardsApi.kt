package spoonacular.recipeCards

import common.BaseRestApiImpl
import common.RemoteContract
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.response.readText
import io.ktor.http.HttpMethod
import io.ktor.http.URLProtocol
import spoonacular.models.SpoonacularRecipeCard

@Suppress("EXPERIMENTAL_API_USAGE")
class RecipeCardsApi(clientEngine: HttpClientEngine): BaseRestApiImpl() {

    /**
     * Http client creation for this request
     * @param clientEngine - real engine
     */
    private val client = HttpClient(clientEngine) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    /**
     * Fetch rockets from space x storage implementation
     */
    suspend fun fetchRecipeCards(ingredients: String): List<SpoonacularRecipeCard> {
        val response = client.get<String> {
            url {
                protocol = URLProtocol.HTTPS
                method = HttpMethod.Get
                host = RemoteContract.spoonacularBaseUrl
                encodedPath = "findByIngredients?number=5&ranking=2&ignorePantry=false&ingredients=$ingredients"
                header("x-rapidapi-host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com")
                header("x-rapidapi-key", "lFMuUx9WZMmshbQDu6H1eiVS2Qmcp1gmvbxjsnXPb9ytoZqhg2")
            }
        }

        val jsonBody = response
        return decodeFromString(jsonBody)
    }
}