package spoonacular.recipeCards

import spoonacular.models.SpoonacularRecipeCard
import kotlinx.serialization.Serializable

@Serializable
data class RecipeCardsFetchResponse(val productions: List<SpoonacularRecipeCard>)
