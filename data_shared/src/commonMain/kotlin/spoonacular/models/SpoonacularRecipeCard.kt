package spoonacular.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SpoonacularRecipeCard(
    @SerialName(value = "id") val rawId: Int,
    @SerialName(value = "title") val name: String,
    @SerialName(value = "image") val imageUrl: String
)