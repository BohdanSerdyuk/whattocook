package common

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

open class BaseRestApiImpl {

    inline fun <reified T> decodeFromString(json: String): T {
        val jsonConfig = Json {
            ignoreUnknownKeys = true
            encodeDefaults = false
            useArrayPolymorphism = true
        }
        val result = jsonConfig.decodeFromString<T>(json)
        return result
    }
}