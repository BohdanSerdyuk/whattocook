package com.bohdanserdyuk.whattocook.di

import com.bohdanserdyuk.whattocook.di.modules.ActivityBuildersModule
import com.bohdanserdyuk.whattocook.di.modules.InteractorsModule
import com.bohdanserdyuk.whattocook.di.modules.UseCasesModule
import com.bohdanserdyuk.whattocook.views.impl.FavoriteRecipeCardsFragment
import com.bohdanserdyuk.whattocook.views.impl.HomeRecipeCardsFragment
import com.bohdanserdyuk.whattocook.views.impl.MainActivity
import com.bohdanserdyuk.whattocook.views.impl.SearchRecipeCardsFragment
import dagger.Component

@Component(
    modules = [InteractorsModule::class,
        ActivityBuildersModule::class,
        UseCasesModule::class]
)
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(fragment: SearchRecipeCardsFragment)
    fun inject(fragment: FavoriteRecipeCardsFragment)
    fun inject(fragment: HomeRecipeCardsFragment)
}