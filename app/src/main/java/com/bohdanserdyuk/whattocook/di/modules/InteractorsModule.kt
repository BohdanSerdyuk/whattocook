package com.bohdanserdyuk.whattocook.di.modules

import com.bohdanserdyuk.whattocook.PreferencesInteractor
import dagger.Module
import dagger.Provides

@Module
class InteractorsModule {

    @Provides
    fun providePreferenceInteractor(): PreferencesInteractor {
        return PreferencesInteractor()
    }
}