package com.bohdanserdyuk.whattocook.di.modules

import com.bohdanserdyuk.whattocook.base.ServiceLocator
import com.bohdanserdyuk.whattocook.features.fetch_image.FetchImageUseCase
import com.bohdanserdyuk.whattocook.features.fetch_labels.FetchLabelsUseCase
import com.bohdanserdyuk.whattocook.features.fetch_recipe_cards.FetchRecipeCardsUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCasesModule {

    @Provides
    fun provideFetchRecipeCardsUseCase(): FetchRecipeCardsUseCase {
        return ServiceLocator.fetchRecipeCardsUseCase
    }

    @Provides
    fun provideFetchLabelsUseCase(): FetchLabelsUseCase {
        return ServiceLocator.fetchLabelsUseCase
    }

    @Provides
    fun provideFetchImageUseCase(): FetchImageUseCase {
        return ServiceLocator.fetchImageUseCase
    }
}