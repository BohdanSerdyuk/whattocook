package com.bohdanserdyuk.whattocook.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.bohdanserdyuk.whattocook.PreferencesInteractor
import com.bohdanserdyuk.whattocook.features.fetch_labels.FetchLabelsUseCase
import com.bohdanserdyuk.whattocook.views.MainView
import com.bohdanserdyuk.whattocook.views.components.liveDatas.events.LabelsLoaded
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@InjectViewState
class MainPresenter(
    val preferencesInteractor: PreferencesInteractor,
    val labelsUseCase: FetchLabelsUseCase
) : MvpPresenter<MainView>() {

    fun cameraClicked() {
        viewState.takePhoto(preferencesInteractor.getPhotoQuality())
    }

    fun photoResult(photo: ByteArray) {
        CoroutineScope(Dispatchers.Main).launch {
            labelsUseCase.invoke(photo, onSuccess = {
                viewState.showSearchResultsFragment()
                viewState.sendBrokerEvent(LabelsLoaded(it.productions))
            }, onFailure = {

            })
        }
    }
}