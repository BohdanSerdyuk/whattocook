package com.bohdanserdyuk.whattocook.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.bohdanserdyuk.whattocook.views.FavoriteRecipeCardsView
import com.bohdanserdyuk.whattocook.views.SearchRecipeCardsView

@InjectViewState
class FavoriteRecipeCardsPresenter(): MvpPresenter<FavoriteRecipeCardsView>()  {

}