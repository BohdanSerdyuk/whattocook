package com.bohdanserdyuk.whattocook.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.bohdanserdyuk.whattocook.features.fetch_image.FetchImageUseCase
import com.bohdanserdyuk.whattocook.features.fetch_recipe_cards.FetchRecipeCardsUseCase
import com.bohdanserdyuk.whattocook.features.models.Label
import com.bohdanserdyuk.whattocook.features.models.RecipeCard
import com.bohdanserdyuk.whattocook.views.SearchRecipeCardsView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@InjectViewState
class SearchRecipeCardsPresenter(
    val recipeCardsUseCase: FetchRecipeCardsUseCase,
    val imageUseCase: FetchImageUseCase
) :
    MvpPresenter<SearchRecipeCardsView>() {

    lateinit var adapterItems: ArrayList<RecipeCard>

    fun onViewCreated() {
        viewState.observeBrokerEvents()
    }

    fun labelsLoaded(labels: List<Label>) {
        CoroutineScope(Dispatchers.Main).launch {
            recipeCardsUseCase.invoke(labels, onSuccess = {
                adapterItems = ArrayList(it.productions)
                viewState.updateRecipesAdapter(adapterItems)
                viewState.showToast(labels.joinToString(", ") { it.name })
            }, onFailure = {
            })
        }
    }

    fun viewHolderBinded(position: Int) {
        CoroutineScope(Dispatchers.Main).launch {
            imageUseCase.invoke(adapterItems[position].imageUrl, {
                viewState.bindImageToViewHolder(position, it.productions.bytes)
            }, {
            })
        }
    }

    fun onRecyclerViewItemClick(adapterPosition: Int) {
        //TODO Implement recipe card data fetching
    }
}