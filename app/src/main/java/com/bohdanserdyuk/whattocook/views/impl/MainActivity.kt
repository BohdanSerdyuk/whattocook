package com.bohdanserdyuk.whattocook.views.impl

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import androidx.navigation.findNavController
import butterknife.ButterKnife
import butterknife.OnClick
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bohdanserdyuk.whattocook.PreferencesInteractor
import com.bohdanserdyuk.whattocook.R
import com.bohdanserdyuk.whattocook.WhattocookApplication
import com.bohdanserdyuk.whattocook.features.fetch_labels.FetchLabelsUseCase
import com.bohdanserdyuk.whattocook.presenters.MainPresenter
import com.bohdanserdyuk.whattocook.views.MainView
import com.bohdanserdyuk.whattocook.views.base.BaseActivity
import com.bohdanserdyuk.whattocook.views.extensions.getPngByteArray
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {
    @Inject
    lateinit var preferencesInteractor: PreferencesInteractor

    @Inject
    lateinit var fetchLabelsUseCase: FetchLabelsUseCase

    var photoQuality = 0

    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    @ProvidePresenter
    fun provideMainPresenter(): MainPresenter {
        return MainPresenter(preferencesInteractor, fetchLabelsUseCase)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as WhattocookApplication).appComponent.inject(this)
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        setSupportActionBar(bottomAppBar)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.bottom_app_bar, menu)
        return true
    }

    override fun observeBrokerEvents() {
        TODO("Not yet implemented")
    }

    override fun showSearchResultsFragment() {
        this.findNavController(R.id.nav_host_fragment)
            .navigate(R.id.searchRecipeCardsFragment)
    }

    override fun takePhoto(quality: Int) {
        photoQuality = quality
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            mainPresenter.photoResult(imageBitmap.getPngByteArray(photoQuality))
        }

    }

    @OnClick(R.id.cameraButton)
    fun cameraClicked() {
        mainPresenter.cameraClicked()
    }

    private companion object {
        const val REQUEST_IMAGE_CAPTURE = 1812
    }
}
