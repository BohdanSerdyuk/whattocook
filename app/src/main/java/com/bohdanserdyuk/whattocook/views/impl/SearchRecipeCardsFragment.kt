package com.bohdanserdyuk.whattocook.views.impl

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bohdanserdyuk.whattocook.R
import com.bohdanserdyuk.whattocook.WhattocookApplication
import com.bohdanserdyuk.whattocook.features.fetch_image.FetchImageUseCase
import com.bohdanserdyuk.whattocook.features.fetch_recipe_cards.FetchRecipeCardsUseCase
import com.bohdanserdyuk.whattocook.features.models.RecipeCard
import com.bohdanserdyuk.whattocook.presenters.SearchRecipeCardsPresenter
import com.bohdanserdyuk.whattocook.views.SearchRecipeCardsView
import com.bohdanserdyuk.whattocook.views.base.BaseFragment
import com.bohdanserdyuk.whattocook.views.components.adapters.SearchRecipeCardsAdapter
import com.bohdanserdyuk.whattocook.views.components.liveDatas.events.LabelsLoaded
import com.bohdanserdyuk.whattocook.views.components.viewModels.WhattocookViewModel
import kotlinx.android.synthetic.main.fragment_search_recipe_cards.*
import javax.inject.Inject


class SearchRecipeCardsFragment : BaseFragment(), SearchRecipeCardsView {

    @Inject
    lateinit var fetchRecipeCardsUseCase: FetchRecipeCardsUseCase

    @Inject
    lateinit var fetchImageUseCase: FetchImageUseCase

    @InjectPresenter
    lateinit var presenter: SearchRecipeCardsPresenter

    @ProvidePresenter
    fun providePresenter(): SearchRecipeCardsPresenter {
        return SearchRecipeCardsPresenter(fetchRecipeCardsUseCase, fetchImageUseCase)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (activity?.application as WhattocookApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun observeBrokerEvents() {
        ViewModelProvider(activity!!).get(WhattocookViewModel::class.java).getMessageContainer()
            .observe(this, Observer {
                if (it is LabelsLoaded) {
                    presenter.labelsLoaded(it.labels)
                }
            })
    }

    override fun bindImageToViewHolder(position: Int, image: ByteArray) {
        val adapter = (recyclerView.adapter!! as SearchRecipeCardsAdapter)
        adapter.items[position].imageBytes = image
        adapter.notifyDataSetChanged()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_recipe_cards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()
    }

    override fun updateRecipesAdapter(recipes: ArrayList<RecipeCard>) {
        recyclerView.layoutManager = GridLayoutManager(activity, 1)
        recyclerView.adapter =
            SearchRecipeCardsAdapter(
                recipes,
                presenter
            )
    }
}
