package com.bohdanserdyuk.whattocook.views

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface MainView: BaseView {
    fun showSearchResultsFragment()
    @StateStrategyType(value = SkipStrategy::class)
    fun takePhoto(quality: Int)
}