package com.bohdanserdyuk.whattocook.views.impl

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bohdanserdyuk.whattocook.R
import com.bohdanserdyuk.whattocook.WhattocookApplication
import com.bohdanserdyuk.whattocook.features.fetch_recipe_cards.FetchRecipeCardsUseCase
import com.bohdanserdyuk.whattocook.features.models.RecipeCard
import com.bohdanserdyuk.whattocook.presenters.HomeRecipeCardsPresenter
import com.bohdanserdyuk.whattocook.presenters.SearchRecipeCardsPresenter
import com.bohdanserdyuk.whattocook.views.HomeRecipeCardsView
import com.bohdanserdyuk.whattocook.views.SearchRecipeCardsView
import com.bohdanserdyuk.whattocook.views.base.BaseFragment
import com.bohdanserdyuk.whattocook.views.components.adapters.SearchRecipeCardsAdapter
import kotlinx.android.synthetic.main.fragment_search_recipe_cards.*
import javax.inject.Inject


class HomeRecipeCardsFragment : BaseFragment(), HomeRecipeCardsView {


    @InjectPresenter
    lateinit var presenter: HomeRecipeCardsPresenter

    @ProvidePresenter
    fun providePresenter(): HomeRecipeCardsPresenter {
        return HomeRecipeCardsPresenter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (activity?.application as WhattocookApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun observeBrokerEvents() {

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home_recipe_cards, container, false)
    }
}
