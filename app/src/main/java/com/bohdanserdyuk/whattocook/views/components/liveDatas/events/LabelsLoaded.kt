package com.bohdanserdyuk.whattocook.views.components.liveDatas.events

data class LabelsLoaded(val labels: List<com.bohdanserdyuk.whattocook.features.models.Label>)