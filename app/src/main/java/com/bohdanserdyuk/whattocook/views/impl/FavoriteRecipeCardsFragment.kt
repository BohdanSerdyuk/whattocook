package com.bohdanserdyuk.whattocook.views.impl

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bohdanserdyuk.whattocook.R
import com.bohdanserdyuk.whattocook.features.models.RecipeCard
import com.bohdanserdyuk.whattocook.models.Recipe
import com.bohdanserdyuk.whattocook.presenters.FavoriteRecipeCardsPresenter
import com.bohdanserdyuk.whattocook.presenters.MainPresenter
import com.bohdanserdyuk.whattocook.views.FavoriteRecipeCardsView
import com.bohdanserdyuk.whattocook.views.base.BaseFragment

class FavoriteRecipeCardsFragment : BaseFragment(), FavoriteRecipeCardsView {

    @InjectPresenter
    lateinit var presenter: FavoriteRecipeCardsPresenter

    @ProvidePresenter
    fun providePresenter(): FavoriteRecipeCardsPresenter {
        return FavoriteRecipeCardsPresenter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite_recipe_cards, container, false)
    }

    override fun observeBrokerEvents() {

    }

    override fun updateRecipesAdapter(recipes: List<RecipeCard>) {
    }
}
