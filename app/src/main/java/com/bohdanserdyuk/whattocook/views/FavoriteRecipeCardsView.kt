package com.bohdanserdyuk.whattocook.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.bohdanserdyuk.whattocook.features.models.RecipeCard
import com.bohdanserdyuk.whattocook.models.Label
import com.bohdanserdyuk.whattocook.models.Recipe

interface FavoriteRecipeCardsView: BaseView {
    fun updateRecipesAdapter(recipes: List<RecipeCard>)
}