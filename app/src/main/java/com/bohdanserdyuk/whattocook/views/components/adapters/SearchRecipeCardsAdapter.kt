package com.bohdanserdyuk.whattocook.views.components.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bohdanserdyuk.whattocook.R
import com.bohdanserdyuk.whattocook.features.models.RecipeCard
import com.bohdanserdyuk.whattocook.presenters.SearchRecipeCardsPresenter
import com.bohdanserdyuk.whattocook.views.components.adapters.viewHolders.RecipeCardHolder

class SearchRecipeCardsAdapter(val items: ArrayList<RecipeCard>, val presenter: SearchRecipeCardsPresenter) :
    RecyclerView.Adapter<RecipeCardHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeCardHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recipe_card, parent, false)
        val viewHolder = RecipeCardHolder(view)
        view.setOnClickListener { presenter.onRecyclerViewItemClick(viewHolder.adapterPosition) }
        return viewHolder
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecipeCardHolder, position: Int) {
        val item = items[position]
        if (item.imageBytes.isEmpty()) {
            presenter.viewHolderBinded(position)
        }
        holder.bind(item)
    }

}

