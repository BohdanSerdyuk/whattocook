package com.bohdanserdyuk.whattocook.views.base

import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.arellomobile.mvp.MvpAppCompatActivity
import com.bohdanserdyuk.whattocook.views.BaseView
import com.bohdanserdyuk.whattocook.views.components.viewModels.WhattocookViewModel


abstract class BaseActivity : MvpAppCompatActivity(), BaseView {

    abstract override fun observeBrokerEvents()

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun sendBrokerEvent(msg: Any) =
        ViewModelProvider(this).get(WhattocookViewModel::class.java).getMessageContainer()
            .setValue(msg)
}
