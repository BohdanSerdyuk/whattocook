package com.bohdanserdyuk.whattocook.views.components.liveDatas

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

/**
 * Container of LiveData that allows to subscribe on LiveData's events separately
 * depend on value class type.
 */
class EventsLiveDataContainer : MutableLiveData<Any>() {

    val map: HashMap<Class<Any>, MutableLiveData<Any>> = HashMap()
    val observers: HashMap<LifecycleOwner, HashSet<Observer<in Any>>> = HashMap()

    /**
     * Setting value to already created LiveData or create new one, depend on [value] type
     */
    override fun setValue(smth: Any) {
        if (!map.keys.contains(smth.javaClass)) {
            map[smth.javaClass] = MutableLiveData()
            observers.forEach { (k, v) -> v.forEach { map[smth.javaClass]?.observe(k, it) } }
        }
        map[smth.javaClass]!!.value = smth
    }

    fun removeObserver(life: LifecycleOwner, value: Class<Any>) {
        if (map[value] != null) {
            map[value]!!.removeObservers(life)
        }
    }

    /**
     * Observe all LiveData's at once
     */
    override fun observe(owner: LifecycleOwner, observer: Observer<in Any>) {
        if (!observers.containsKey(owner)) {
            observers[owner] = HashSet()
        }
        observers[owner]!!.add(observer)
        map.values.forEach { it.observe(owner, observer) }
    }
}