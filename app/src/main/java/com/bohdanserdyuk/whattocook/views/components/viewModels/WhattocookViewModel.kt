package com.bohdanserdyuk.whattocook.views.components.viewModels

import androidx.lifecycle.ViewModel
import com.bohdanserdyuk.whattocook.views.components.liveDatas.EventsLiveDataContainer

class WhattocookViewModel: ViewModel() {

    private var messageContainer = EventsLiveDataContainer()

    fun getMessageContainer(): EventsLiveDataContainer {
        return messageContainer
    }

    override fun onCleared() {
        super.onCleared()
        messageContainer = EventsLiveDataContainer()
    }
}