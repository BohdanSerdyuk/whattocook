package com.bohdanserdyuk.whattocook.views.components.adapters.viewHolders

import android.graphics.Bitmap
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bohdanserdyuk.whattocook.features.models.RecipeCard
import com.bohdanserdyuk.whattocook.views.extensions.toBitmap
import kotlinx.android.synthetic.main.recipe_card.view.*

class RecipeCardHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(recipeCard: RecipeCard) {
        view.recipe_name.text = recipeCard.name
        val image = view.image
        if (recipeCard.imageBytes.isNotEmpty()) {
            image.setImageBitmap(
                recipeCard.imageBytes.toBitmap()
            )
        }
    }
}