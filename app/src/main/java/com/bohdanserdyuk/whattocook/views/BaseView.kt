package com.bohdanserdyuk.whattocook.views

import com.arellomobile.mvp.MvpView

interface BaseView: MvpView {
    fun showToast(message: String)
    fun observeBrokerEvents()
    fun sendBrokerEvent(msg: Any)
}