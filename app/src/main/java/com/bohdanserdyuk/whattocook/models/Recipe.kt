package com.bohdanserdyuk.whattocook.models

data class Recipe(
    val name: String,
    val recipe: String
)