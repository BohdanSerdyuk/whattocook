package com.bohdanserdyuk.whattocook.models

data class Label(
    val name: String
)