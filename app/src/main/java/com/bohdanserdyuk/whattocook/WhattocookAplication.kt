package com.bohdanserdyuk.whattocook

import android.app.Application
import com.bohdanserdyuk.whattocook.di.AppComponent
import com.bohdanserdyuk.whattocook.di.DaggerAppComponent

class WhattocookApplication: Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().build()
    }
}