package com.bohdanserdyuk.whattocook.features.fetch_recipe_cards

import com.bohdanserdyuk.whattocook.features.models.RecipeCard

data class ResponseModel(val productions: List<RecipeCard>)