package com.bohdanserdyuk.whattocook.features.fetch_recipe_cards

import com.bohdanserdyuk.whattocook.base.Either
import com.bohdanserdyuk.whattocook.base.Failure
import com.bohdanserdyuk.whattocook.base.Success
import com.bohdanserdyuk.whattocook.base.UseCase
import com.bohdanserdyuk.whattocook.features.models.Label
import com.bohdanserdyuk.whattocook.features.models.RecipeCard
import com.bohdanserdyuk.whattocook.features.models.SourceType
import spoonacular.recipeCards.RecipeCardsApi

class FetchRecipeCardsUseCase(private val restApi: RecipeCardsApi) : UseCase<ResponseModel, List<Label>>() {
    override suspend fun run(params: List<Label>): Either<Exception, ResponseModel> {
        return try {
            Success(
               ResponseModel(restApi.fetchRecipeCards(params.joinToString(separator = ",") { it.name }).map { RecipeCard(it.rawId.toString(), it.name, it.imageUrl, SourceType.SPOONACULAR) })
            )
        } catch (e: Exception) {
            Failure(e)
        }
    }
}