package com.bohdanserdyuk.whattocook.features.models

data class Image(
    val bytes: ByteArray
)
