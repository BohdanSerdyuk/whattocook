package com.bohdanserdyuk.whattocook.features.models

data class RecipeCard(
    val id: String,
    val name: String,
    val imageUrl: String,
    val source: SourceType
) {
    var imageBytes: ByteArray = ByteArray(0)
}

enum class SourceType {
    SPOONACULAR
}