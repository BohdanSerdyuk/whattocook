package com.bohdanserdyuk.whattocook.features.models

data class Label(
    val name: String
)
