package com.bohdanserdyuk.whattocook.features.fetch_image

import com.bohdanserdyuk.whattocook.base.Either
import com.bohdanserdyuk.whattocook.base.Failure
import com.bohdanserdyuk.whattocook.base.Success
import com.bohdanserdyuk.whattocook.base.UseCase
import com.bohdanserdyuk.whattocook.features.models.Image
import image_loader.image_loader.ImageLoaderApi

class FetchImageUseCase(val restApi: ImageLoaderApi) : UseCase<ResponseModel, String>() {
    override suspend fun run(params: String): Either<Exception, ResponseModel> {
        return try {
            Success(
                ResponseModel(Image(restApi.fetchImage(params).image.bytes))
            )
        } catch (e: Exception) {
            Failure(e)
        }
    }
}