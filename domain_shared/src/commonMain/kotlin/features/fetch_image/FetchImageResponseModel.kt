package com.bohdanserdyuk.whattocook.features.fetch_image

import com.bohdanserdyuk.whattocook.features.models.Image

data class ResponseModel(val productions: Image)