package com.bohdanserdyuk.whattocook.features.fetch_labels

import com.bohdanserdyuk.whattocook.base.Either
import com.bohdanserdyuk.whattocook.base.Failure
import com.bohdanserdyuk.whattocook.base.Success
import com.bohdanserdyuk.whattocook.base.UseCase
import com.bohdanserdyuk.whattocook.features.models.Label
import edamam.recipeCards.LabelsDetectionApi
import google_cloud.labelsDetection.*

class FetchLabelsUseCase(val restApi: LabelsDetectionApi) : UseCase<ResponseModel, ByteArray>() {
    override suspend fun run(params: ByteArray): Either<Exception, ResponseModel> {
        return try {
            Success(
                ResponseModel(
                    restApi.fetchDetectedLabels(LabelsDetectionRequest(params))
                        .map { Label(it.description) })
            )
        } catch (e: Exception) {
            Failure(e)
        }
    }
}