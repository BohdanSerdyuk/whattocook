package com.bohdanserdyuk.whattocook.features.fetch_labels

import com.bohdanserdyuk.whattocook.features.models.Label

data class ResponseModel(val productions: List<Label>)