package com.bohdanserdyuk.whattocook.base

import com.bohdanserdyuk.whattocook.features.fetch_image.FetchImageUseCase
import com.bohdanserdyuk.whattocook.features.fetch_labels.FetchLabelsUseCase
import com.bohdanserdyuk.whattocook.features.fetch_recipe_cards.FetchRecipeCardsUseCase
import edamam.recipeCards.LabelsDetectionApi
import image_loader.image_loader.ImageLoaderApi
import io.ktor.client.engine.HttpClientEngine
import spoonacular.recipeCards.RecipeCardsApi
import kotlin.native.concurrent.ThreadLocal

@ThreadLocal
object ServiceLocator {

    private val recipeCardsApi by lazy { RecipeCardsApi(PlatformServiceLocator.httpClientEngine) }

    private val labelsDetectionApi by lazy { LabelsDetectionApi(PlatformServiceLocator.httpClientEngine) }

    private val imageLoaderApi by lazy {
        ImageLoaderApi(
            PlatformServiceLocator.httpClientEngine
        )
    }

    val fetchImageUseCase: FetchImageUseCase
        get() = FetchImageUseCase(restApi = imageLoaderApi)

    val fetchRecipeCardsUseCase: FetchRecipeCardsUseCase
        get() = FetchRecipeCardsUseCase(restApi = recipeCardsApi)

    val fetchLabelsUseCase: FetchLabelsUseCase
        get() = FetchLabelsUseCase(restApi = labelsDetectionApi)
}

/**
 * Contains some expected dependencies for the [ServiceLocator] that have to be resolved by Android/iOS.
 */
expect object PlatformServiceLocator {
    val httpClientEngine: HttpClientEngine
}