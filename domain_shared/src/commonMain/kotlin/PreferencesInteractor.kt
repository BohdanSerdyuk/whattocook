package com.bohdanserdyuk.whattocook

expect fun getPhotoQualityAsync(): Int

class PreferencesInteractor {
    fun getPhotoQuality(): Int {
        return getPhotoQualityAsync()
    }
}