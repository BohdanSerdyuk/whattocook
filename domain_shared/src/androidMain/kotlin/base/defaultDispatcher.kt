package com.bohdanserdyuk.whattocook.base

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

actual val defaultDispatcher: CoroutineContext
    get() = Dispatchers.Default
actual val uiDispatcher: CoroutineContext
    get() = Dispatchers.Main